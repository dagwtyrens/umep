# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=UMEP
qgisMinimumVersion=2.0
description=Urban Multi-scale Environmental Predictor

version=0.2.9
author=Fredrik Lindberg - Sue Grimmond - Niklas Krave - Leena Jarvi - Helen Ward - Shiho Onomura - Christoph Kent - Andy Gabey - Frans Olofson - Ting Sun
email=fredrikl@gvc.gu.se

about=UMEP is an extensive plugin designed for urban climate in general and climate sensitive planning applications in particular. The plugin consist of a pre-processor, a processor and a post-processor. This plugin is a collaboration between University of Gothenburg, University of Helsinki and University of Reading.
      
tracker=https://bitbucket.org/fredrik_ucg/umep/issues
repository=https://bitbucket.org/fredrik_ucg/umep

# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
changelog=
	0.2.9 : Improvments in ImageMorphGrid, SOLWEIGAnalyzer and the SUEWS plugins.
	0.2.8 : Bug fixes in SUEWSSimple and Metdataprocessor.
	0.2.7 : Issue #12 fixed. Animation included in SOLWEIGAnalyzer.
	0.2.6 : New plugin: SOLWEIGAnalyzer. Bug fix to cope with multiple days in SOLWEIG
	0.2.5 : Fixed a blocking bug where python dependencies stop the plugin to start
	0.2.4 : New pre-prcessing plugin, TreeGenerator. General bug fixes.
	0.2.3 : Major updates of the FootPrintModel and the SOLWEIG model.
	0.2.2 : RunControl.nml edited online with Bitbucket
	0.2.1 : SOLWEIG added. Minos bug fixes
	0.2.0 : LONG TERM RELEASE. Bug fixes. New Plugins: WATCHData and GreaterQF (London)
	0.1.5 : BETA. Inclusion of SUEWS v2016b. Bug fixes.
	0.1.4 : LONG TERM RELEASE. Layouts added, many bug fixes, more documentation. New plugin: SUEWSPrepare
	0.1.3 : Bug in SuewsSimple fixed.
	0.1.2 : Bug in Image morphometric where building heights were underestimated is now fixed.
	0.1.1 : Bug fix issue #2 and #3. Data-folders are now created if missing. 
	0.1   : Official realease, VERSION 0.1 BETA
	

# Tags are comma separated with spaces allowed
tags=urban climate, urban planning

homepage=http://www.urban-climate.net/umep/
category=Plugins
icon=Icons/icon_umep.png

# experimental flag
experimental=True

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

