# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=Tree Generator
qgisMinimumVersion=2.0
description=This plugin generates a vegetation canopy DSM and Trunk zone DSM from point data
version=0.1
author=Fredrik Lindberg
email=fredrikl@gvc.gu.se

about=This plugin generates a vegetation canopy DSM and Trunk zone DSM from point vector data

tracker=https://bitbucket.org/fredrik_ucg/umep/issues
repository=https://bitbucket.org/fredrik_ucg/umep
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=

homepage=http://www.urban-climate.net/umep/UMEP
category=Plugins
icon=icon.png
# experimental flag
experimental=True

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

