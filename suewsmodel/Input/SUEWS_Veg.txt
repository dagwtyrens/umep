1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22	23	24	25	26	27	28	29	30	31	32	33				
Code	AlbedoMin	AlbedoMax	Emissivity	StorageMin	StorageMax	WetThreshold	StateLimit	DrainageEq	DrainageCoef1	DrainageCoef2	SoilTypeCode	SnowLimPatch	BaseT	BaseTe	GDDFull	SDDFull	LAIMin	LAIMax	MaxConductance  	LAIEq	LeafGrowthPower1	LeafGrowthPower2	LeafOffPower1	LeafOffPower2	OHMCode_SummerWet	OHMCode_SummerDry	OHMCode_WinterWet	OHMCode_WinterDry	ESTMCode	AnOHM_Cp	AnOHM_Kk	AnOHM_Ch				
300	0.1	0.1	0.98	1.3	1.3	1.3	1.3	2	0.013	1.71	70	190	5	10	300	-450	4	5.1	7.4	1	0.04	0.001	-1.5	0.0015	200	200	200	200	200	100000	1.2	4	!	ET	Helsinki	Jarvi et al (2014)
330	0.16	0.16	0.98	0.3	0.8	0.8	0.8	2	0.013	1.71	70	190	5	10	300	-450	1	5.5	11.7	1	0.04	0.001	-1.5	0.0015	200	200	200	200	200	100000	1.2	4	!	DT	Helsinki	Jarvi et al (2014)
360	0.19	0.19	0.93	1.9	1.9	1.9	1.9	3	10	3	70	190	5	10	300	-450	1.6	5.9	40	1	0.04	0.001	-1.5	0.0015	200	200	200	200	200	100000	1.2	4	!	IG	Helsinki	Jarvi et al (2014)
361	0.19	0.19	0.93	1.9	1.9	1.9	1.9	2	0.13	1.71	70	190	5	10	300	-450	1.6	5.9	33.1	1	0.04	0.001	-1.5	0.0015	200	200	200	200	200	100000	1.2	4	!	UG	Helsinki	Jarvi et al (2014)
551	0.10	0.10	0.98	1.30	1.30	1.80	1.30	2	0.013	1.71	55661	-999	6	11	300	-450	4	5.1	7.4	1	0.04	0.001	-1.5	0.0015	2011	2011	2012	2012	810	100000	1.2	4	!	EveTr		Sw (Ward et al. 2016)
552	0.12	0.18	0.98	0.30	0.80	1.00	0.80	2	0.013	1.71	55661	-999	6	11	300	-450	1	5.5	11.7	1	0.04	0.001	-1.5	0.0015	2011	2011	2012	2012	811	100000	1.2	4	!	DecTr		Sw (Ward et al. 2016)
553	0.18	0.21	0.93	1.90	1.90	2.00	1.90	2	0.013	1.71	55661	-999	6	11	300	-450	1.6	5.9	33.1	1	0.04	0.001	-1.5	0.0015	2011	2011	2012	2012	812	100000	1.2	4	!	Grass		Sw (Ward et al. 2016)
661	0.10	0.10	0.98	1.30	1.30	1.80	1.30	2	0.013	1.71	55661	-999	6	11	300	-450	4	5.1	7.4	1	0.04	0.001	-1.5	0.0015	2011	2011	2012	2012	810	100000	1.2	4	!	EveTr		Kc (Ward et al. 2016)
662	0.12	0.18	0.98	0.30	0.80	1.00	0.80	2	0.013	1.71	55661	-999	6	11	300	-450	1	5.5	11.7	1	0.04	0.001	-1.5	0.0015	2011	2011	2012	2012	811	100000	1.2	4	!	DecTr		Kc (Ward et al. 2016)
663	0.18	0.21	0.93	1.90	1.90	2.00	1.90	2	0.013	1.71	55661	-999	6	11	300	-450	1.6	5.9	33.1	1	0.04	0.001	-1.5	0.0015	2011	2011	2012	2012	812	100000	1.2	4	!	Grass		Kc (Ward et al. 2016)
-9																																				
-9																																				
																																				
																																				
																																				
																																				
551		Increased	Default	Default			Default	Default			Code	NOT USED	Increase by 1 degC		Default		Default		Default	Default					"""Vegetation (average)"" - used for Vs, Sm"								!	EveTr		
552		Default	Default	Default			Default	Default			Code	NOT USED	Increase by 1 degC		Default		Default		Default	Default					"""Vegetation (average)"" - used for Vs, Sm"								!	DecTr		
553		Increased	Default	Default			Default	Default (unirrigated)			Code	NOT USED	Increase by 1 degC		Default		Default		Default	Default					"""Vegetation (average)"" - used for Vs, Sm"								!	Grass		
